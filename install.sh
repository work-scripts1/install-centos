#!/bin/bash
function message() {
	if ! [[ $2 ]]; then
		# Green bold text
		echo -e '\033[1;32m'$1'\033[0m'
	else
		# Green, red and blue text
		echo -e '\033[1;32m'$1'\033[1;31m >> \033[1;34m'$2'\033[0m'
	fi
}

#   Update system
sudo dnf update -y && sudo dnf upgrade -y

#   Add user
name=$(sysadmin) 
sudo adduser $name && usermod -aG wheel $name
sudo echo ' $name ALL=(ALL) ALL' >> /etc/sudoers

#   Install Apps
sudo dnf install mc git vim wget yum-utils -y

#   Configurate firewall rules
#sudo firewall-cmd --zone=public --permanent --add-port=8080/tcp
sudo firewall-cmd --zone=public --permanent --add-service=http
sudo firewall-cmd --zone=public --permanent --add-service=https
sudo firewall-cmd --zone=public --add-masquerade --permanent
sudo firewall-cmd --reload

#   Install Docker
message 'Removing old Docker instances'
sudo dnf remove -y docker \
  docker-client \
  docker-client-latest \
  docker-common \
  docker-latest \
  docker-latest-logrotate \
  docker-logrotate \
  docker-selinux \
  docker-engine-selinux \
  docker-engine

message 'Adding Docker repositories to local list'
sudo dnf -y install dnf-plugins-core
sudo dnf config-manager \
  --add-repo \
  https://download.docker.com/linux/fedora/docker-ce.repo

message 'Installing Docker on the machine'
sudo dnf install -y docker-ce \
  docker-ce-cli \
  containerd.io \
  docker-compose

message 'Starting Docker services'
sudo systemctl enable --now docker

message 'Post-installation steps'
sudo groupadd docker
sudo usermod -aG docker $name
newgrp docker

#   Install docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo usermod -aG docker $name

#   Instal zsh
message 'Installing zsh'
sudo dnf install -y zsh

message 'Installing zsh' 'Installing oh-my-zsh'
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh && exit)"

message 'Post-install' 'Changing default shell to zsh'
sudo dnf install -y util-linux-user
sudo chsh -s /usr/bin/zsh

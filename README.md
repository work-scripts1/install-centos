**Install Centos base Apps**

This script installs:

- Midnigt commander
- Removes the old version of Docker and installs the new one
- Docker-compose
- Adds custom a user to the system (you can them it as you want)
- Configuring firewall rules, open http/https ports permanently
- Zsh
